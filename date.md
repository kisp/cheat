Display the current date/time:

$ date [+FORMAT]

like:

Display the year on 4 characters, month and day of month in both 2-2
$ date +%Y%m%d
$ date +%H:%M:%S
$ date +'%A, %B %d, %Y'

Display the seconds since 1970
$ date +%s

Set the system date:

$ date [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]

For FORMAT character sequences, see strftime.
