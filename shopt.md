The Shopt Builtin
=================

This builtin allows you to change additional shell optional behavior.
 shopt [-pqsu] [-o] [optname]

-p : output to be displayed in a form that may be reused as input. Other options have the following meanings:
-s Enable (set) each optname.
-u Disable (unset) each optname.
-q Suppresses normal output; the return status indicates whether the optname is set or unset. If multiple optname arguments are given with -q, the return status is zero if all optnames are enabled; non-zero otherwise.
-o Restricts the values of optname to be those defined for the -o option to the set builtin (see The Set Builtin).

If either -s or -u is used with no optname arguments, the display is limited to those options which are set or unset, respectively.Unless otherwise noted, the shopt options are disabled (off) by default. The return status when listing options is zero if all optnames are enabled, non-zero otherwise. When setting or unsetting options, the return status is zero unless an optname is not a valid shell option.

The list of shopt options is
----------------------------

* autocd : If set, a command name that is the name of a directory is executed as if it were the argument to the cd command. This option is only used by interactive shells.
* cdable_vars : If this is set, an argument to the cd builtin command that is not a directory is assumed to be the name of a variable whose value is the directory to change to.
* cdspell : If set, minor errors in the spelling of a directory component in a cd command will be corrected. The errors checked for are transposed characters, a missing character, and a character too many. If a correction is found, the corrected path is printed, and the command proceeds. This option is only used by interactive shells.
* checkhash : If this is set, Bash checks that a command found in the hash table exists before trying to execute it. If a hashed command no longer exists, a normal path search is performed.
* checkjobs : If set, Bash lists the status of any stopped and running jobs before exiting an interactive shell. If any jobs are running, this causes the exit to be deferred until a second exit is attempted without an intervening command (see Job Control). The shell always postpones exiting if any jobs are stopped.
* checkwinsize : If set, Bash checks the window size after each command and, if necessary, updates the values of LINES and COLUMNS.
* cmdhist : If set, Bash attempts to save all lines of a multiple-line command in the same history entry. This allows easy re-editing of multi-line commands.
* compat31 : If set, Bash changes its behavior to that of version 3.1 with respect to quoted arguments to the conditional command伍西s 伍西=~伍西 operator.
* compat32 : If set, Bash changes its behavior to that of version 3.2 with respect to locale-specific string comparison when using the 伍西[[伍西 conditional command伍西s 伍西<伍西 and 伍西>伍西 operators. Bash versions prior to bash-4.0 use ASCII collation and strcmp(3); bash-4.1 and later use the current locale伍西s collation sequence and strcoll(3).
* compat40 : If set, Bash changes its behavior to that of version 4.0 with respect to locale-specific string comparison when using the 伍西[[伍西 conditional command伍西s 伍西<伍西 and 伍西>伍西 operators (see previous item) and the effect of interrupting a command list.
* compat41 : If set, Bash, when in posix mode, treats a single quote in a double-quoted parameter expansion as a special character. The single quotes must match (an even number) and the characters between the single quotes are considered quoted. This is the behavior of POSIX mode through version 4.1. The default Bash behavior remains as in previous versions.
* dirspell : If set, Bash attempts spelling correction on directory names during word completion if the directory name initially supplied does not exist.
* dotglob : If set, Bash includes filenames beginning with a 伍西.伍西 in the results of filename expansion.
* execfail : If this is set, a non-interactive shell will not exit if it cannot execute the file specified as an argument to the exec builtin command. An interactive shell does not exit if exec fails.
* expand_aliases : If set, aliases are expanded as described below under Aliases, Aliases. This option is enabled by default for interactive shells.
* extdebug : If set, behavior intended for use by debuggers is enabled:

The -F option to the declare builtin (see Bash Builtins) displays the source file name and line number corresponding to each function name supplied as an argument.
If the command run by the DEBUG trap returns a non-zero value, the next command is skipped and not executed.
If the command run by the DEBUG trap returns a value of 2, and the shell is executing in a subroutine (a shell function or a shell script executed by the . or source builtins), a call to return is simulated.
BASH_ARGC and BASH_ARGV are updated as described in their descriptions (see Bash Variables).
Function tracing is enabled: command substitution, shell functions, and subshells invoked with ( command ) inherit the DEBUG and RETURN traps.
Error tracing is enabled: command substitution, shell functions, and subshells invoked with ( command ) inherit the ERR trap.

* extglob : If set, the extended pattern matching features described above (see Pattern Matching) are enabled.
* extquote : If set, $'string' and $"string" quoting is performed within ${parameter} expansions enclosed in double quotes. This option is enabled by default.
* failglob : If set, patterns which fail to match filenames during filename expansion result in an expansion error.
* force_fignore : If set, the suffixes specified by the FIGNORE shell variable cause words to be ignored when performing word completion even if the ignored words are the only possible completions. See Bash Variables, for a description of FIGNORE. This option is enabled by default. 
* globstar If set, the pattern 伍西**伍西 used in a filename expansion context will match all files and zero or more directories and subdirectories. If the pattern is followed by a 伍西/伍西, only directories and subdirectories match.
* gnu_errfmt If set, shell error messages are written in the standard GNU error message format.
* histappend If set, the history list is appended to the file named by the value of the HISTFILE variable when the shell exits, rather than overwriting the file.
* histreedit If set, and Readline is being used, a user is given the opportunity to re-edit a failed history substitution.
* histverify If set, and Readline is being used, the results of history substitution are not immediately passed to the shell parser. Instead, the resulting line is loaded into the Readline editing buffer, allowing further modification. 
* hostcomplete If set, and Readline is being used, Bash will attempt to perform hostname completion when a word containing a 伍西@伍西 is being completed (see Commands For Completion). This option is enabled by default. 
* huponexit If set, Bash will send SIGHUP to all jobs when an interactive login shell exits (see Signals).  
* interactive_comments Allow a word beginning with 伍西#伍西 to cause that word and all remaining characters on that line to be ignored in an interactive shell. This option is enabled by default.
* lastpipe If set, and job control is not active, the shell runs the last command of a pipeline not executed in the background in the current shell environment.
* lithist If enabled, and the cmdhist option is enabled, multi-line commands are saved to the history with embedded newlines rather than using semicolon separators where possible.
* login_shell The shell sets this option if it is started as a login shell (see Invoking Bash). The value may not be changed.
* mailwarn If set, and a file that Bash is checking for mail has been accessed since the last time it was checked, the message "The mail in mailfile has been read" is displayed.
* no_empty_cmd_completion If set, and Readline is being used, Bash will not attempt to search the PATH for possible completions when completion is attempted on an empty line.
* nocaseglob If set, Bash matches filenames in a case-insensitive fashion when performing filename expansion.
* nocasematch If set, Bash matches patterns in a case-insensitive fashion when performing matching while executing case or [[ conditional commands.
* nullglob If set, Bash allows filename patterns which match no files to expand to a null string, rather than themselves.
* progcomp If set, the programmable completion facilities (see Programmable Completion) are enabled. This option is enabled by default.
* promptvars If set, prompt strings undergo parameter expansion, command substitution, arithmetic expansion, and quote removal after being expanded as described below (see Printing a Prompt). This option is enabled by default.
* restricted_shell The shell sets this option if it is started in restricted mode (see The Restricted Shell). The value may not be changed. This is not reset when the startup files are executed, allowing the startup files to discover whether or not a shell is restricted.
* shift_verbose If this is set, the shift builtin prints an error message when the shift count exceeds the number of positional parameters.
* sourcepath If set, the source builtin uses the value of PATH to find the directory containing the file supplied as an argument. This option is enabled by default.
* xpg_echo If set, the echo builtin expands backslash-escape sequences by default.

The return status when listing options is zero if all optnames are enabled, non-zero otherwise. When setting or unsetting options, the return status is zero unless an optname is not a valid shell option.

