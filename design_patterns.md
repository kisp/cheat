Java design patterns in a nutshell
==================================

* ref : http://www.tutorialspoint.com/design_pattern/design_pattern_quick_guide.htm
* Common platform for developers
* Best practices
* Types : Creational, Structural, Behavioral, J2EE

Overview
========

Creational
----------
* Factory: create object without exposing the creation logic to the client and refer to newly created object using a common interface
* Abstract factory: pattern works around a super-factory which creates other factories
* Singleton: involves a single class which is responsible to creates own object while making sure that only single object get created
* Builder: builds a complex object using simple objects and using a step by step approach
* Prototype: involves implementing a prototype interface which tells to create a clone of the current object. This pattern is used when creation of object directly is costly

Structural
----------
* Adapter: works as a bridge between two incompatible interfaces. A real life example could be a case of card reader which acts as an adapter between memory card and a laptop
* Bridge:  used where we need to decouple an abstraction from its implementation
* Filter: enables developers to filter a set of objects, using different criteria, chaining them in a decoupled way through logical operations
* Composite: used where we need to treat a group of objects in similar way as a single object. Composite pattern composes objects in term of a tree structure to represent part as well as whole hierarchy
* Decorator: allows to add new functionality an existing object without altering its structure
* Facade: hides the complexities of the system and provides an interface to the client using which the client can access the system
* Flyweight: primarily used to reduce the number of objects created, to decrease memory footprint and increase performance.
* Proxy: a class represents functionality of another class

Behavioral
----------
* Chain of Responsibility: creates a chain of receiver objects for a request. This pattern decouples sender and receiver of a request based on type of request
* Command:  A request is wrapped under an interface as command and passed to invoker object.
* Interpreter: This pattern involves implementing a expression interface which tells to interpret a particular context. This pattern is used in SQL parsing, symbol processing engine etc.
* Iterator: used to get a way to access the elements of a collection object in sequential manner without any need to know its underlying representation
* Mediator: provides a mediator class which normally handles all the communications between different classes and supports easy maintainability of the code by loose coupling
* Memento: used to reduce where we want to restore state of an object to a previous state
* Observer : used when there is one to many relationship between objects such as if one object is modified, its depenedent objects are to be notified automatically
* State : a class behavior changes based on its state
* Null Object :
* Strategy :
* Template :
* Visitor :
* MVC :
* Business Delegate :
* Composite Entity :
* Data Access Object : 
* Front Controller :
* Intercepting Filter :
* Service Locator :
* Transfer Object :
