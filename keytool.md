Keytool :
=======

java keytool to use keystores

Listing certificates defined in a keystore :
    $ keytool.exe -list -keystore <keystore> -storepass <password>

Exporting certificate for curl :

    $ keytool.exe -export -alias <keyname> -keystore <keystore> -storepass <password> > <filename>.pem

----
