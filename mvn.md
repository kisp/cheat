Maven
=====

* mvn -Dmaven.test.skip=true => skip tests
* mvn -X => To debug (lifecycles):
* mvn help:effective-pom => To print effective pom:
* mvn -U => update remotes


Other Tips
----------

* To setup local repo use switch -Dmaven.repo.local=localrepo
* You can force to update local snapshots with the switch -U
* Basic lifecycle phases : validate > compile > test > package > integration-test > verify > install > deploy
  * only need to specify last phase to execute chain.
  * in a multi module scenario : on all modules it will executed under
* http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
