Usage (optionals marked with *)
===============================
grails [environment]* [target] [arguments]*

Examples:
grails dev run-app
grails create-app books

Available Targets (type grails help 'target-name' for more info):
grails add-proxy
grails bootstrap
grails bug-report
grails clean
grails clean-gwt-modules
grails clear-proxy
grails compile
grails compile-gwt-modules
grails compile-i18n
grails console
grails create-app
grails create-controller
grails create-domain-class
grails create-filters
grails create-gwt-action
grails create-gwt-event
grails create-gwt-i18n
grails create-gwt-module
grails create-gwt-page
grails create-hibernate-cfg-xml
grails create-integration-test
grails create-plugin
grails create-script
grails create-service
grails create-tag-lib
grails create-unit-test
grails dependency-report
grails doc
grails generate-all
grails generate-controller
grails generate-gwt-rpc
grails generate-views
grails help
grails init
grails install-dependency
grails install-plugin
grails install-templates
grails integrate-with
grails interactive
grails list-plugin-updates
grails list-plugins
grails package
grails package-plugin
grails plugin-info
grails publish-github
grails release-plugin
grails remove-proxy
grails run-app
grails run-gwt-client
grails run-script
grails run-war
grails schema-export
grails set-proxy
grails set-version
grails shell
grails stats
grails test-app
grails tomcat
grails uninstall-plugin
grails upgrade
grails war

Arguments:
--non-interactive : use this parameter to use the default yes answer to install plugins.
